package com.shamison.config;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by shamison on 15/02/17.
 */
public class Config {
	private Configuration conf;
	private FMLPreInitializationEvent event;

	private String consumerKey;
	private String consumerSecret;
	private String accessTokenKey;
	private String accessTokenSecret;

	private String screenNameColor;
	private String nameColor;
	private String tweetColor;
	private String otherColor;


	public Config(FMLPreInitializationEvent event) {
		this.event = event;
	}

	public void loadConf() {
		conf = new Configuration(event.getSuggestedConfigurationFile());
		conf.load();

		consumerKey = conf.getString("SYSTEM", "consumerKey", "xxxxxxxxxx", "consumerKey");
		consumerSecret = conf.getString("SYSTEM", "consumerSecret", "xxxxxxxxxx", "consumerSecret");
		accessTokenSecret = conf.getString("SYSTEM", "accessTokenSecret", "xxxxxxxxxx", "accessTokenSecret");
		accessTokenKey = conf.getString("SYSTEM", "accessTokenKey", "xxxxxxxxxx", "accessTokenKey");

		nameColor = conf.getString("SYSTEM", "nameColor", "GREEN", "名前の色");
		screenNameColor = conf.getString("SYSTEM", "screenNameColor", "GREEN", "IDの色");
		tweetColor = conf.getString("SYSTEM", "tweetColor", "WHITE", "ツイートの色");
		otherColor = conf.getString("SYSTEM", "otherColor", "GREEN", "その他の色");
		conf.save();
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public String getAccessTokenKey() {
		return accessTokenKey;
	}

	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}

	public String getScreenNameColor() {
		return screenNameColor;
	}

	public String getNameColor() {
		return nameColor;
	}

	public String getTweetColor() {
		return tweetColor;
	}

	public String getOtherColor() {
		return otherColor;
	}


}
