package com.shamison;

import com.shamison.commands.StreamCommand;
import com.shamison.commands.TweetCommand;
import com.shamison.config.Config;
import com.shamison.twitter.Tweet;
import com.shamison.twitter.TwitterConf;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import twitter4j.TwitterStream;

/**
 * Created by shamison on 15/02/17.
 */
@Mod(modid = TuiHaiMOD.MODID, version = TuiHaiMOD.VERSION)
public class TuiHaiMOD {
	public static final String MODID = "tuihaimod";
	public static final String VERSION = "alpha0.1";
	private TwitterStream ts;
	private TwitterConf tc;
	private static Config conf;
	private Tweet tw;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		conf = new Config(event);
		conf.loadConf();
	}


	@Mod.EventHandler
	public void onServerStart(FMLServerStartingEvent event) {
		tc = new TwitterConf(conf);
		tw = new Tweet(tc.getTwitter());
		event.registerServerCommand(new StreamCommand(tc.getTwitterStream()));
		event.registerServerCommand(new TweetCommand(tw));
	}
	public static Config getConf() {
		return conf;
	}
}
