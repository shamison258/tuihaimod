package com.shamison.chat;

import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;

import static net.minecraft.util.EnumChatFormatting.WHITE;

/**
 * Created by shamison on 15/02/22.
 */
public class ChatUtils {
	ChatComponentText text;
	ChatStyle style;

	public ChatUtils(String str) {
		this.text = new ChatComponentText(str);
		this.style = new ChatStyle();
		style.setColor(WHITE);
		text.setChatStyle(style);

	}

	public void setColor(EnumChatFormatting e) {
		style.setColor(e);
		text.setChatStyle(style);
	}

	public void setText(ChatComponentText text) {
		this.text = text;
	}

	public void cleanText() {
		this.text = null;
	}

	public ChatComponentText getText() {
		return text;
	}
}
