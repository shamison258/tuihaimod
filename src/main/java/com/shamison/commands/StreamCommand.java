package com.shamison.commands;

import com.shamison.twitter.StreamUtils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import twitter4j.TwitterStream;


/**
 * Created by shamison on 15/02/17.
 */
public class StreamCommand extends CommandBase {
	private TwitterStream ts;
	private StreamUtils su;

	public StreamCommand(TwitterStream ts) {
		su = new StreamUtils(ts);
	}

	@Override
	public String getName() {
		return "stream";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return null;
	}

	@Override
	public void execute(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			su.setup(sender);
			if (args[0].equals("start")) {
				su.start(sender);
			} else if (args[0].equals("stop")) {
				su.stop(sender);
			}
		}
	}

	@Override
	public boolean canCommandSenderUse(ICommandSender sender) {
		return true;
	}
}
