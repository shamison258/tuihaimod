package com.shamison.commands;

import com.shamison.twitter.Tweet;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Created by shamison on 15/02/22.
 */
public class TweetCommand extends CommandBase {
	private Tweet tweet;
	private StatusUpdate statusUpdate;
	private StringBuilder sb;

	public TweetCommand(Tweet tweet) {
		this.tweet = tweet;
		sb = new StringBuilder();

	}

	@Override
	public String getName() {
		return "tweet";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return null;
	}

	@Override
	public void execute(ICommandSender sender, String[] args) throws CommandException {
		sender.addChatMessage(new ChatComponentText("ツイートします..."));
		String msg;
		for (String arg : args )
			sb.append(arg+ " ");
		msg = sb.toString().trim();
		if (tweet.update(msg)){
			sender.addChatMessage(new ChatComponentText("ツイートしました！"));
		}else{
			sender.addChatMessage(new ChatComponentText("ツイートできませんでした..."));
		}
	}

	@Override
	public boolean canCommandSenderUse(ICommandSender sender) {
		return true;
	}
}
