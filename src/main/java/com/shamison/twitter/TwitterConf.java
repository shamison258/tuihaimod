package com.shamison.twitter;

import com.shamison.config.Config;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by shamison on 15/02/17.
 */
public class TwitterConf {
	private TwitterStream twitterStream;
	private Twitter twitter;
	private ConfigurationBuilder cb = null;
	private Configuration config = null;


	public TwitterConf(Config conf) {
		cb = new ConfigurationBuilder();
		cb.setDebugEnabled(false)
				.setOAuthConsumerKey(conf.getConsumerKey())
				.setOAuthConsumerSecret(conf.getConsumerSecret())
				.setOAuthAccessToken(conf.getAccessTokenKey())
				.setOAuthAccessTokenSecret(conf.getAccessTokenSecret());
		config = cb.build();
		twitterStream = new TwitterStreamFactory(config).getInstance();
		twitter = new TwitterFactory(config).getInstance();
	}

	public TwitterStream getTwitterStream() {
		return twitterStream;
	}

	public Twitter getTwitter() {
		return twitter;
	}
}
