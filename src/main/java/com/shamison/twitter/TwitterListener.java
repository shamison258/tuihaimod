package com.shamison.twitter;

import com.shamison.TuiHaiMOD;
import com.shamison.chat.ChatUtils;
import com.shamison.config.Config;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import twitter4j.Status;
import twitter4j.UserStreamAdapter;

/**
 * Created by shamison on 15/02/17.
 */
public class TwitterListener extends UserStreamAdapter {
	private ICommandSender sender;
	private Config conf;
	private EnumChatFormatting e;

	private ChatUtils name = null;
	private ChatUtils other = null;
	private ChatUtils screenName = null;
	private ChatUtils tweet = null;

	private ChatComponentText sendText;


	public TwitterListener(ICommandSender sender) {
		this.sender = sender;
		conf = TuiHaiMOD.getConf();
	}


	@Override
	public void onStatus(Status status) {
		setColor(status);
		formatText();
		/*
		 * name(@screenname):
		 * tweet
		 */
		sender.addChatMessage(sendText);
		sender.addChatMessage(tweet.getText());
	}

	private void formatText() {
		sendText = new ChatComponentText("");

		sendText.appendSibling(name.getText());
		setOtherText("(@");
		sendText.appendSibling(screenName.getText());
		setOtherText("):");
	}

	public void setOtherText(String text){
		other.setText(new ChatComponentText(text));
		other.setColor(EnumChatFormatting.valueOf(conf.getOtherColor()));
		sendText.appendSibling(other.getText());

	}

	private void setColor(Status status) {
		//name color setting
		name = new ChatUtils(status.getUser().getName());
		name.setColor(EnumChatFormatting.valueOf(conf.getNameColor()));

		//screenname color setting
		screenName = new ChatUtils(status.getUser().getScreenName());
		screenName.setColor(EnumChatFormatting.valueOf(conf.getScreenNameColor()));

		//tweet color setting
		tweet = new ChatUtils(status.getText());
		tweet.setColor(EnumChatFormatting.valueOf(conf.getTweetColor()));

		other = new ChatUtils("");
		other.setColor(EnumChatFormatting.valueOf(conf.getOtherColor()));
	}
}
