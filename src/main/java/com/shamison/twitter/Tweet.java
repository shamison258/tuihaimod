package com.shamison.twitter;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Created by shamison on 15/02/22.
 */
public class Tweet {
	private Twitter twitter;

	public Tweet(Twitter twitter) {
		this.twitter = twitter;
	}

	public boolean update(String tweet){
		try {
			Status status = twitter.updateStatus(tweet);
			return true;
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return false;
	}
}
