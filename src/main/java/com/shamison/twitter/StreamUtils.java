package com.shamison.twitter;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import twitter4j.TwitterStream;

/**
 * Created by shamison on 15/02/21.
 */
public class StreamUtils {
	private TwitterStream ts;
	private static TwitterListener listener;

	public StreamUtils(TwitterStream ts) {
		this.ts = ts;
	}

	public void setup(ICommandSender sender) {
		this.listener = new TwitterListener(sender);

	}

	public void start(ICommandSender sender) {
		stop(sender);
		ts.addListener(listener);
		ts.user();
		String msg = "ストリーミングを開始します。少々お待ちください...";
		sender.addChatMessage(new ChatComponentText(msg));
	}

	public void stop(ICommandSender sender) {
		String msg = "ストリーミングを停止します...";
		sender.addChatMessage(new ChatComponentText(msg));
		ts.clearListeners();
		ts.cleanUp();
		ts.shutdown();
		msg = "停止しました！";
		sender.addChatMessage(new ChatComponentText(msg));
	}
}
